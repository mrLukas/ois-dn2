var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var geslaKanalov = [['Skedenj', undefined]];


exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    obdelajPridruzitevKanaluZGeslom(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    socket.on('uporabnikiRef', function() {
      refreshUsers(socket);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    sendToPrivate(socket);
  });
};

function sendToPrivate(socket){
  socket.on('posljiZasebno', function(send) {
    var to = send.vzd;
    var mes = send.mes;
    var poslano = false;
    var tto = to.substring(1, to.length - 1);
        
    
    for(var i in vzdevkiGledeNaSocket){
      if(vzdevkiGledeNaSocket[i]==tto){
        mes = mes.substring(1, mes.length - 1);
        var message = vzdevkiGledeNaSocket[socket.id] + ": " + mes;
        io.sockets.socket(i).emit('sporocilo', {besedilo: message});
        poslano = true;
      }
    }
    
    if(!poslano){
      var sporo = "Sporočilo <" + mes + "> uporabniku z vzdevkom <" + to + "> ni bilo mogoče posredovati.";
      socket.emit('sporocilo', {besedilo:sporo});
    }
    
  });
}

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}
var kanalGlobal;
function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  kanalGlobal = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}


function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket){
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    var kanal2 = kanal.novKanal;
    var channels = io.sockets.manager.rooms;
    var gK;
    for(var st=0; st<geslaKanalov.length; st++){
      if(geslaKanalov[st][0]==kanal2)
        gk = geslaKanalov[st][1];
    }
    
    var alije = false;
    for(var i in channels){
      i = i.substr(1,i.length);
      if(i == kanal2){
        alije = true;
      }
    }
    console.log(gk +kanal2 + alije);
    if(alije){
      if(gk===undefined){
        socket.leave(trenutniKanal[socket.id]);
        pridruzitevKanalu(socket, kanal2);
      }
      else{
        var bes = "Pridružitev v kanal <"+kanal2+"> ni bilo uspešno, ker je kanal zaklenjen z geslom!";
        socket.emit('sporocilo', {besedilo: bes});
      }
    }
    else{
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, kanal2);
    }
  });
}

function obdelajPridruzitevKanaluZGeslom(socket) {
  socket.on('pridruzitevZahtevaGeslo', function(input) {
    var kanal2 = input.novKanal;
    var ge = input.geslo;
    var channels = io.sockets.manager.rooms;
    
    var gK;
    for(var st=0; st<geslaKanalov.length; st++){
      if(geslaKanalov[st][0]==kanal2)
        gk = geslaKanalov[st][1];
    }
    
    var alije = false;
    for(var i in channels){
      i = i.substr(1,i.length);
      if(i == kanal2){
        alije = true;
      }
    }
    if(alije){
      if(gk===undefined){
        var bes = "Izbrani kanal <"+kanal2+"> je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev <"+kanal2+"> ali zahtevajte kreiranje kanala z drugim imenom.";
        socket.emit('sporocilo', {besedilo: bes});
      }
      else{
        if(gk==ge){
          socket.leave(trenutniKanal[socket.id]);
          pridruzitevKanalu(socket, kanal2);
        }
        else{
          var bes = "Pridružitev v kanal <"+kanal2+"> ni bilo uspešno, ker je geslo napačno!";
          socket.emit('sporocilo', {besedilo: bes});
        }
      }
    }
    else{
      geslaKanalov.push([kanal2,ge]);
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, kanal2);
    }
    
  });
}

function refreshUsers(socket){
  var tab = "";
  var uporabnikiNaKanalu = io.sockets.clients(kanalGlobal);
  for(var i in uporabnikiNaKanalu){
    var index = uporabnikiNaKanalu[i].id;
    tab += vzdevkiGledeNaSocket[index] + "</br>";
  }
  socket.emit('prikljuceni', {odgovor:tab});
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
  socket.emit()
}