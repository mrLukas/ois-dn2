var wink = '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png" width="15" height="15" alt="wink.png">';
var smiley = '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png" width="15" height="15" alt="smiley.png">';
var like = '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png" width="15" height="15" alt="like.png">';
var kiss = '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png" width="15" height="15" alt="kiss.png">';
var sad = '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png" width="15" height="15" alt="sad.png">';

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').append(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


function replaceAll(find, replace, str) {
  if(str.indexOf(find) == -1){
    return str;
  }
  else{
    var string = str.replace(find,replace);
    return replaceAll(find,replace,string);
  }
}



var sporr;

function procesirajVnosUporabnika(klepetApp, socket) {
  sporr = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;
  
  
  if (sporr.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporr);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {

    // Dodaj smeskote za tega uporabnika
    sporr = replaceAll(";)",wink,sporr);
    sporr = replaceAll(":)",smiley,sporr);
    sporr = replaceAll("(y)",like,sporr);
    sporr = replaceAll(":*",kiss,sporr);
    sporr = replaceAll(":(",sad,sporr);

      // Preberi datoteko in filtriraj
      
      $.ajax({
          type: "GET",
          url: "swearWords.csv",
          dataType: "text",
          success: function(data) {
            changeText(data);
          }
       });
    
    
    function changeText(data){
      var allTextLines = data;
      var entries = allTextLines.split(',');
      var spremeniBAD = [];
      
      for(var i = 0; i < entries.length; i++){
        spremeniBAD.push(new RegExp('\\b('+entries[i]+')\\b', 'gmi'));
      }
      
      for(var i = 0; i < entries.length; i++){
        //spremeni v **
        var spremeniV = entries[i].replace(/./gmi, '*');
        sporr = sporr.replace(entries[i], spremeniV);
      }
      
      var kanalDIV = $('#kanal').text();
      kanalDIV = kanalDIV.split(' ');
      console.log("*"+kanalDIV[2]+"*");
      klepetApp.posljiSporocilo(kanalDIV[2], sporr);
     $('#sporocila').append(divElementEnostavniTekst(sporr));
     $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    }

  }

  // Resetiraj vnosno polje
  $('#poslji-sporocilo').val('');
}

var socket = io.connect(); 

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  var user;
  
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      user = rezultat.vzdevek;
      } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(user + " @ " + rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  socket.on('sporocilo', function (sporocilo) {
    var Nsporocilo = sporocilo.besedilo;
    Nsporocilo = replaceAll(";)",wink,Nsporocilo);
    Nsporocilo = replaceAll(":)",smiley,Nsporocilo);
    Nsporocilo = replaceAll("(y)",like,Nsporocilo);
    Nsporocilo = replaceAll(":*",kiss,Nsporocilo);
    Nsporocilo = replaceAll(":(",sad,Nsporocilo);
    
    var novElement = $('<div style="font-weight: bold"></div>').append(Nsporocilo);
    $('#sporocila').append(novElement);
  });
  

  socket.on('prikljuceni', function (users) {
     $('#seznam-prijavljenih').empty();
     $('#seznam-prijavljenih').append(users.odgovor);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabnikiRef');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
  
 
});