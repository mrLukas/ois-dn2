var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};
Klepet.prototype.spremeniKanalZGeslom = function(kanal,geslo) {
  this.socket.emit('pridruzitevZahtevaGeslo', {
    novKanal: kanal,
    geslo:geslo
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;
  

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();

      if(besede.length == 1){
        this.spremeniKanal(besede[0]);
      }
      else{
        var uk = besede.join(' ');
        uk = uk.substring(1,uk.length-1);
        uk = uk.split('> <');
        this.spremeniKanalZGeslom(uk[0], uk[1]);
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var vzdevek1 = besede[0];
      besede.shift();
      var messagePrivate = besede.join(' ');
      this.socket.emit('posljiZasebno', {vzd:vzdevek1, mes:messagePrivate} );
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};